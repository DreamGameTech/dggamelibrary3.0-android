[English](./README.md) | 简体中文 
## DGLibrary is the SDK of DreamGame Technology's game products, which is used by platform providers to connect with our games.

## The SDK includes DG Baccarat, Dragon Tiger, Niu Niu, Roulette, Sic Bo, Color Disc, and Golden Flower Games

### Docking method
	(1)Currently the SDK exists in the plugin project in order to reduce some unnecessary troubles.
	(2) Integrate the host project into its own project (referred to as the host project), package the plugin project (plug-in project) into an apk, and place it in its own remote server for the host project to download and update dynamically.
    (3) Replace the replugin-host-gradle-2.3.3.jar in the plugin project with the local replugin-host-gradle-2.3.3.jar, so that the interface after calling the plugin is horizontal by default
Note (Important): You can download the https://github.com/Qihoo360/RePlugin project first to see the effect, and basically provide solutions in this project for the problems that arise during access.

### Document change log

Version number | Modified content | Modified date
----|------|----
1.0.0 | 1.sdk plug-in | 2020-08-07
1.0.1 | 1. Added fried golden flowers and color plates | 2020-09-01
 

### Environment configuration
##### host project
  1. Add dependency in host_demo\build.gradle classpath "com.qihoo360.replugin:replugin-host-gradle:2.3.3"
  
  2. app\build.gradle
	 1. Add dependency  implementation "com.qihoo360.replugin:replugin-host-lib:2.3.3" 
     1. Add plugin configuration information above dependencies, not at the beginning of this file     
		```
			apply plugin: `replugin-host-gradle`    		
			repluginHostConfig {  
				useAppCompat = false  
			}
		```
	 1.  add in android node  
		```
			dataBinding {
					enabled = true
			}
		```
3. Application inherits RePluginApplication and adds those methods like hostDemo
 4. Add permissions
```
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
  
    <uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```
##### plugin project 
1. Add dependency classpath "com.qihoo360.replugin:replugin-host-gradle:2.3.3" in plugin_demo\build.gradle
 2. app\build.gradle  
	1. Add dependency  implementation "com.qihoo360.replugin:replugin-host-lib:2.3.3"
	1. Add plugin information at the bottom
		```
		apply plugin: 'replugin-plugin-gradle'
		repluginPluginConfig {
			pluginName = "dgplugin"   /*  The plugin alias is defined by yourself*/
			hostApplicationId = "com.replugin.demo.host"  /*  宿主host  应用id*/
			hostAppLauncherActivity = "com.replugin.demo.host.MainActivity"
		}
		```
 3. Define aliases in AndroidManifest.xml  
	```
				<meta-data
						android:name="com.qihoo360.plugin.name"
						android:value="dgplugin" />
	```		
 4. Add permission  
	```
					<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
		<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
		<uses-permission android:name="android.permission.INTERNET"/>
		<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
		<uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
		<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
		<uses-permission android:name="android.permission.VIBRATE"/>
		<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
	```
	
 5.To package this plugin project, remember to check V1 V2 when packaging.
 

##### Release
1. Encrypt the packaged plug-in apk (required, HostUtil), and change the way to obtain the plug-in apk to download. For example, to obtain the configuration information of the plugin whose version number is 1 for the first time, see setPLugin1,
Then decrypt and install it in MainActivity, and call it successfully in StartGameActivity.
	```
        Intent intent = new Intent();
        // An alias is defined in the plugin's AndroidManifest.xml, which must be started as an alias
        intent.setComponent(new ComponentName(pluginName, "**.**.BridgeActivity"));// Please modify it to the corresponding plug-in project BridgeActivity package path
         
        intent.putExtra("minBet", 10);// The minimum value of the balance, when it is less than this value, you cannot bet, 0 has no limit
        intent.putExtra("token", LoginUtil.token);
        intent.putExtra("gameId", gameId);
        intent.putExtra("domains", LoginUtil.getDomains());
        intent.putExtra("hideBarrage", false);// Whether to hide the barrage
        boolean startFlg = RePlugin.startActivityForResult(StartGameActivity.this, intent, REQUEST_CODE_DEMO5);
	```    

2. About uninstalling the plug-in: as long as you enter the game or just install the plug-in, then uninstalling it will not take effect immediately, it will uninstall itself when you enter again. If the plug-in is installed before, and the app is re-entered without running the plug-in, the uninstallation can take effect immediately.  

3. Regarding plugin aliases, please keep the same in AndroidManifest.xml and build.gradle of plugin project and PluginConstant.pluginAlias ​​of host project in these three places.

##### Hint
1. targetSdkVersion does not support version 29 and above.
2. Tips for configuring gradle

android version | gradle | gradle plugin
----|------|----
25 | gradle-4.1-all | 3.0.1 cannot be a later version
26+ | gradle-4.4-all.zip   | 3.1.0
+| gradle-4.6-all.zip  | 3.2.0
+| gradle-4.10.1-all.zip   | 3.3.0
+| gradle-5.1.1-all.zip   | 3.4.0
+| gradle-5.4.1-all.zip   | 3.5.0 