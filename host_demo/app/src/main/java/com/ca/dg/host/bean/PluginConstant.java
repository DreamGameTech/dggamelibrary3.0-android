package com.ca.dg.host.bean;

public class PluginConstant {

    //链接
    public final static String plugin_url = "plugin_url";
    //md5 值
    public final static String plugin_md5 = "plugin_md5";

    public final static String plugin_size = "plugin_size";

    // 已经下载的文件的名字， 加密后的， 并且未进行解密
    public final static String plugin_local_path = "plugin_local_path";


    public final static String pluginAlias = "dgplugin";

    private boolean pluginNeedStart = false;

    private static final PluginConstant instance = new PluginConstant();

    private PluginConstant() { }

    public static PluginConstant getInstance() {
        return instance;
    }

    public boolean isPluginNeedStart() {
        return pluginNeedStart;
    }

    public void setPluginNeedStart() {
        pluginNeedStart = true;
    }



}
