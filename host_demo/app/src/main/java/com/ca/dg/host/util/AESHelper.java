package com.ca.dg.host.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * <p>
 * 这是用于android　　加密　解密apk 文件的代码
 */

public class AESHelper {
    public static final String VIPARA = "0102030405060708";
    private static final String TAG = AESHelper.class.getSimpleName();

    public static void ma22in(String... a) {
        //加密                           这个key 可以通过从后台获取来保证安全//  16位 ，  通过通过 md5 来检测 加密解密后的文件
        boolean encryptFile = AESHelper.encryptFile("0q33k1ngpzxlp70n0q33k1ngpzxlp70n", "/Users/Patton/Documents/pro/dev/plugin_demo2/app/release/demo1.apk", "/Users/Patton/Documents/pro/dev/plugin_demo2/app/release/demo42.apk");
        // 解密                                     这个key 可以通过从后台获取来保证安全
        boolean decryptFile = AESHelper.decryptFile("0q33k1ngpzxlp70n0q33k1ngpzxlp70n", "/Users/Patton/Documents/pro/dev/plugin_demo2/app/release/demo42.apk", "/Users/Patton/Documents/pro/dev/plugin_demo2/app/release/demo53.apk");
    }


    /**
     * 初始化 AES Cipher
     *
     * @param sKey 16位 长度，
     * @param cipherMode
     * @return
     */
    private static Cipher initAESCipher(String sKey, int cipherMode) {
        //创建Key gen
        KeyGenerator keyGenerator = null;
        Cipher cipher = null;
        try {
            IvParameterSpec zeroIv = new IvParameterSpec(VIPARA.getBytes());
            SecretKeySpec key = new SecretKeySpec(sKey.getBytes(), "AES");
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(cipherMode, key, zeroIv);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvalidKeyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvalidAlgorithmParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return cipher;
    }


    /**
     * 对文件进行AES加密  encryptFile
     *
     * @param key
     * @param sourceFilePath
     * @param destFilePath
     * @return
     */
    public static boolean encryptFile(String key, String sourceFilePath, String destFilePath) {
        boolean encryptFlg = true;

        FileInputStream in = null;
        FileOutputStream out = null;

        File sourceFile = new File(sourceFilePath);
        File destFile = new File(destFilePath);

        if (!sourceFile.exists() || !sourceFile.isFile()) {
            return !encryptFlg;
        }

        try {
            if (!destFile.getParentFile().exists() && !destFile.getParentFile().mkdirs()) {
                return !encryptFlg;
            }

            if (!destFile.createNewFile()) {
                return !encryptFlg;
            }

            in = new FileInputStream(sourceFile);
            out = new FileOutputStream(destFile);

            Cipher cipher = initAESCipher(key, Cipher.ENCRYPT_MODE);
            //以加密流写入文件
            CipherInputStream cipherInputStream = new CipherInputStream(in, cipher);
            byte[] cache = new byte[1024];
            int nRead = 0;
            while ((nRead = cipherInputStream.read(cache)) != -1) {
                out.write(cache, 0, nRead);
                out.flush();
            }
            cipherInputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            return !encryptFlg;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            return !encryptFlg;
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return encryptFlg;
    }


    /**
     * AES方式解密文件  decryptFile
     *
     * @param key  16 位长度，最好采用从后台获取这个key 的方式
     * @param sourceFilePath
     * @param destFilePath
     * @return
     */
    public static boolean decryptFile(String key, String sourceFilePath, String destFilePath) {

        boolean decryptFlg = true;
        FileInputStream in = null;
        FileOutputStream out = null;

        File sourceFile = new File(sourceFilePath);
        File destFile = new File(destFilePath);

        if (!sourceFile.exists() || !sourceFile.isFile()) {
            Log.e("file", "源文件不存在 " );
            return !decryptFlg;
        }

        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }

        try {
            if (destFile.exists()) {
                if (!destFile.delete()) {
                    Log.e("file", "删除文件失败");
                    return !decryptFlg;
                }
            }
            if (!destFile.createNewFile()) {
                Log.e("file", "创建文件失败 请先申请权限");
                return !decryptFlg;
            }

            in = new FileInputStream(sourceFile);
            out = new FileOutputStream(destFile);

            Cipher cipher = initAESCipher(key, Cipher.DECRYPT_MODE);
            CipherOutputStream cipherOutputStream = new CipherOutputStream(out, cipher);
            byte[] buffer = new byte[1024];
            int r;
            while ((r = in.read(buffer)) >= 0) {
                cipherOutputStream.write(buffer, 0, r);
            }
            cipherOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return !decryptFlg;
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                if (out != null)
                    out.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return decryptFlg;
    }


    public static String getFileMd5(String path) {
        BigInteger bi = null;
        try {
            byte[] buffer = new byte[8192];
            int len = 0;
            MessageDigest md = MessageDigest.getInstance("MD5");
            File f = new File(path);
            FileInputStream fis = new FileInputStream(f);
            while ((len = fis.read(buffer)) != -1) {
                md.update(buffer, 0, len);
            }
            fis.close();
            byte[] b = md.digest();
            bi = new BigInteger(1, b);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bi.toString(16);
    }
}
