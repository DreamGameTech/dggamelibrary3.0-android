package com.ca.dg.host;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OptionAskDialog{

    private static Object lock = new Object();

    private  static  boolean isVisiable = false;

    /**
     @param context        上下文
     @param title          标题
     @param content        内容
     @param btnCancleText  取消按钮文本
     @param btnSureText    确定按钮文本
     @param touchOutside   外部取消
     @param cancleListener 取消监听
     @param sureListener   确定监听
     @return
     */
    public synchronized static CustomizAlertDialog showDialog(Context context,
                                                              String title,
                                                              String content,
                                                              String btnCancleText,
                                                              String btnSureText,
                                                              boolean touchOutside,
                                                              DialogInterface.OnClickListener cancleListener,
                                                              DialogInterface.OnClickListener sureListener) {
        synchronized (lock) {
            if (isVisiable) {
                return null;
            }
            isVisiable = true;
            CustomizAlertDialog customizAlertDialog = new CustomizAlertDialog();
            customizAlertDialog.showDialog(
                    context,
                    title,
                    content,
                    btnCancleText,
                    btnSureText,
                    touchOutside,
                    cancleListener,
                    sureListener);
            return customizAlertDialog;
        }
    }

    public static class CustomizAlertDialog {

        TextView mTextView;

        AlertDialog mDialog;

        public  AlertDialog showDialog(Context context,
                                       String title,
                                       String content,
                                       String btnCancleText,
                                       String btnSureText,
                                       boolean touchOutside,
                                       DialogInterface.OnClickListener cancleListener,
                                       DialogInterface.OnClickListener sureListener) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
           final AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(touchOutside);
            dialog.setCancelable(false);

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    isVisiable = false;
                }
            });

            View view = View.inflate(context, R.layout.dialog_alert_ios, null);
            //标题
            TextView tvTitle = (TextView)view.findViewById(R.id.tv_alert_title);
            //内容
            TextView tvContent = (TextView)view.findViewById(R.id.tv_alert_content);

            mTextView = tvContent;
            //取消按钮
            Button buttonCancle = (Button)view.findViewById(R.id.btn_alert_cancel);
            //确定按钮
            Button buttonOk = (Button)view.findViewById(R.id.btn_alert_ok);
            //线
            View viewLine = view.findViewById(R.id.v_alert_line);

            if (TextUtils.isEmpty(title)) {
                tvTitle.setVisibility(View.GONE);
            } else {
                tvTitle.setText(title);
            }

            tvContent.setText(TextUtils.isEmpty(content) ? "" : content);

            if (TextUtils.isEmpty(btnCancleText)) {
                buttonCancle.setVisibility(View.GONE);
                viewLine.setVisibility(View.GONE);
            } else {
                buttonCancle.setText(btnCancleText);
            }

            buttonOk.setText(TextUtils.isEmpty(btnSureText) ? "确定" : btnSureText);
            final AlertDialog dialogFinal = dialog;
            final DialogInterface.OnClickListener finalCancleListener = cancleListener;
            final DialogInterface.OnClickListener finalSureListener = sureListener;
            buttonCancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalCancleListener == null){
                        dialog.dismiss();
                    }else {
                        finalCancleListener.onClick(dialogFinal, DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    finalSureListener.onClick(dialogFinal, DialogInterface.BUTTON_POSITIVE);
                }
            });

            //设置背景透明,去四个角
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();
            dialog.getWindow().setLayout( 580, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog.setContentView(view);
            mDialog = dialog;
            return dialog;
        }

        public void setText(String text) {
            mTextView.setText(text);
        }
    }
}