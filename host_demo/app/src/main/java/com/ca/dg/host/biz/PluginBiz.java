package com.ca.dg.host.biz;


import com.ca.dg.host.ShareUtil;
import com.ca.dg.host.bean.PluginConstant;
import com.ca.dg.host.util.MD5;

/**
 * 描述：
 * Created by Aiden on 2020/8/22
 */
public class PluginBiz {

    public static boolean checkPluginMd5(String path) {
        String md5 = ShareUtil.get(PluginConstant.plugin_md5, "");
        String fileMD5 = MD5.getFileMD5(path);
        return md5.equals(fileMD5);
    }

}
