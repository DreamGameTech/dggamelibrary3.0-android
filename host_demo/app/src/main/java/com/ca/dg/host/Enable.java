package com.ca.dg.host;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class Enable  extends BaseObservable {

    @Bindable
    boolean enable = true;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
        notifyPropertyChanged(BR.enable);
    }
}
