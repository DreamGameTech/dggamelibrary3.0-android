package com.ca.dg.host;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ca.dg.host.bean.PluginConstant;
import com.ca.dg.host.databinding.ActivityStartGameBinding;
import com.ca.dg.host.util.LoginUtil;
import com.qihoo360.replugin.RePlugin;


public class StartActivity extends Activity {

    private String TAG = "Start0Activity";

    /**
     * demo5 是plugin 项目打包出来 的apk
     */

    private ActivityStartGameBinding mBinding;

    private Enable enable;

    private TextView versionTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start_game);
        enable = new Enable();
        mBinding.setAble(enable);
        findViewById(R.id.token).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginUtil.getToken(StartActivity.this);
            }
        });
        versionTv = (TextView) findViewById(R.id.tv_version);
        getVersion(null);
    }

    public void getVersion(View v) {
        int pluginVersion = RePlugin.getPluginVersion(PluginConstant.pluginAlias);
        versionTv.setText("插件版本:" + pluginVersion);
    }

    public void startPlugin(View v) {

        int gameId = 1;

        if (v.getId() == R.id.btn_start_bull) {
            gameId = 7;
        } else if (v.getId() == R.id.btn_start_sicbo) {
            gameId = 5;
        } else if (v.getId() == R.id.btn_start_roullete) {
            gameId = 4;
        } else if (v.getId() == R.id.btn_start_dragon) {
            gameId = 3;
        } else if (v.getId() == R.id.btn_start_baccart) {
            gameId = 1;
        } else if (v.getId() == R.id.btn_start_sedie) {
            gameId = 14;
        } else if (v.getId() == R.id.btn_start_threecard) {
            gameId = 11;
        }

        final int gameid = gameId;

        boolean pluginInstalled = RePlugin.isPluginInstalled(PluginConstant.pluginAlias);
        if (!pluginInstalled) {
            // 跳转这个下载界面
            Toast.makeText(StartActivity.this, "插件未安装", Toast.LENGTH_SHORT).show();
            OptionAskDialog.showDialog(StartActivity.this, "确定下载?",
                    "下载将花费一定时间和流量",
                    "取消", "确认", false, null,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(StartActivity.this, DGLoadPluginActivity.class);
                            intent.putExtra("gameId", gameid);
                            startActivity(intent);
                        }
                    });

            return;
        }

        Intent intent = new Intent();
        // 在插件的 AndroidManifest.xml 里定义了别名， 这里必须以别名的方式启动
        intent.setComponent(new ComponentName(PluginConstant.pluginAlias, "com.ca.dg.activity.BridgeActivity"));
        if (LoginUtil.token == null) {
            Toast.makeText(StartActivity.this, "请先获取token", Toast.LENGTH_SHORT).show();
            return;
        }
        startAnim();
        intent.putExtra("minBet", 10);
        intent.putExtra("token", LoginUtil.token);
        intent.putExtra("gameId", gameId);
        intent.putExtra("language", 3);
        intent.putExtra("domains", LoginUtil.getDomains());
        intent.putExtra("hideBarrage", false);

        if (PluginConstant.getInstance().isPluginNeedStart()) {
            Toast.makeText(StartActivity.this, "app need restart", Toast.LENGTH_SHORT).show();
        } else {

            int version = RePlugin.getPluginVersion(PluginConstant.pluginAlias);
            int remotePluginVersion = 1;//TODO
            if (version < remotePluginVersion) {
                Toast.makeText(StartActivity.this, "app need restart", Toast.LENGTH_SHORT).show();
                RePlugin.uninstall(PluginConstant.pluginAlias);
                PluginConstant.getInstance().setPluginNeedStart();
                return;
            }

            boolean startFlg = RePlugin.startActivityForResult(StartActivity.this, intent, REQUEST_CODE_DEMO5);
            if (!startFlg) {
                Toast.makeText(StartActivity.this, "start game fail", Toast.LENGTH_SHORT).show();
                boolean flg = RePlugin.uninstall(PluginConstant.pluginAlias);
                PluginConstant.getInstance().setPluginNeedStart();
            }
        }
        mHandler.sendEmptyMessage(0);
    }

    private static final int REQUEST_CODE_DEMO5 = 0x011;
    private static final int RESULT_CODE_DEMO5 = 0x012;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_DEMO5 && resultCode == RESULT_CODE_DEMO5) {
            // 这里主要用来模拟 获取返回的参数
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);
                    Log.i(TAG, String.format("%s %s (%s)", key,
                            value.toString(), value.getClass().getName()));
                }
            }
        }
    }

    private void startAnim() {
        enable.setEnable(false);
    }

    // 主线程中新建一个handler
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 0) {
                enable.setEnable(true);
            }
        }
    };
}
