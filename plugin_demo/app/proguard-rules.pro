# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\tools\eclipse\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html
#
# Add any project specific keep options here:
-optimizationpasses 5   #指定执行几次优化 默认执行一次
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-ignorewarnings
-allowaccessmodification
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep class android.support.v4.** { *; }
-keep interface android.support.v4.app.** { *; }
-dontwarn android.support.v4.**

-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.preference.Preference
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.annotation.**
-keep public class * extends android.support.v7.**
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep public class com.android.vending.licensing.ILicensingService
-keep class android.support.** {*;}

-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-dontwarn android.support.v7.**

-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

-keepattributes LineNumberTable,SourceFile
-keep class com.bugtags.library.** {*;}
-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.bugtags.library.vender.**
-dontwarn com.bugtags.library.**

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
    public void get*(...);
}

-keep class cn.nodemedia.** { *; }

-keepclasseswithmembernames class * {     # 保持 native 方法不被混淆
    native <methods>;
}
-keep public class * implements android.os.Parcelable

-keep public class * implements java.io.Serializable

-dontwarn android.databinding.**
-keep class android.databinding.** { *; }

-keep class com.opensource.svgaplayer.** { *; }
-keep class **.R$* {*;}
-keep class com.ca.dg.biz.RoadBiz{*;}
-keep class com.ca.dg.util.UIHelper{*;}
-keep class com.ca.dg.app.** { *; }
-keep class com.ca.dg.activity.DGLoadUtil{ *; }
-keep class com.ca.dg.databinding.** { *; }
-keep public class com.ca.dg.R$*{
    public static final int *;
}
-keep public class com.ca.dg.model.** { *; }
-keep public class com.ca.dg.viewModel.** { *; }
-keep public class com.ca.dg.view.custom.bet.ImgsLocal { *; }
-keep public class com.ca.zxing.** { *; }
-keep public class com.ca.dg.constant.GameId{ *; }

-keepattributes *Annotation*
-keepattributes Signature,InnerClasses
-keepclasseswithmembers class io.netty.** { *; }
-keepnames class io.netty.** { *; }
-keep interface com.ca.dg.activity.DGDataListener{ *; }
-keep class com.google.protobuf.** { *; }
-keep interface com.google.protobuf.** { *; }
-keep public class * extends android.view.View {*;}
-keep public class * extends android.app.Activity{ public *;}
-keep public class * extends android.support.v4.app.Fragment {*;}
-keep public class * extends android.app.Fragment {*;}
-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.** { *; }
-keep interface com.alibaba.fastjson.** { *; }

-keep class tv.danmaku.** { *; }

-keepclassmembers public class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}


-keepclassmembers class * {
    void *(*Event);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * extends java.lang.annotation.Annotation
-keepclasseswithmembernames class * {
    native <methods>;
}
