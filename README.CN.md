English|[简体中文](./README.CN.md) 
## DGLibrary 是DreamGame Technology 公司游戏产品的SDK，供平台商对接我司游戏使用

## 该SDK包含DG 百家乐、龙虎、牛牛、轮盘、骰宝、色碟、炸金花游戏

### 对接方式
	(1)目前该SDK存在于plugin项目，以便减少一些不必要的麻烦。
	(2)将host项目融合到自己的项目(简称宿主项目)中， 将plugin项目(插件项目)进行打包成apk,并放置在自己的远程服务器中，供宿主项目下载动态下载更新。  
    (3)将插件项目中的replugin-host-gradle-2.3.3.jar替换本地的replugin-host-gradle-2.3.3.jar，以便调用插件后的界面默认是  
	 注(重要):可以先下载https://github.com/Qihoo360/RePlugin 项目看看效果，接入中出现的问题，在这个项目中基本上都提供了解决方案。
	
横屏的。

### 文档修改日志

版本号 | 修改内容 | 修改日期
----|------|----
1.0.0 | 1.sdk插件化    | 2020-08-07
1.0.1 | 1.增加炸金花、色碟    | 2020-09-01
 

### 环境配置
##### host 项目
  1. 在host_demo\build.gradle 添加依赖 classpath "com.qihoo360.replugin:replugin-host-gradle:2.3.3"
  
  2. app\build.gradle
	 1. 添加依赖  implementation "com.qihoo360.replugin:replugin-host-lib:2.3.3" 
     1. 在dependencies 上方添加插件配置信息，别在这个文件的开头位置     
		```
			apply plugin: `replugin-host-gradle`    		
			repluginHostConfig {  
				useAppCompat = false  
			}
		```
	 1.  在 android 节点添加  
		```
			dataBinding {
					enabled = true
			}
		```
 3. Application 继承 RePluginApplication，仿照hostDemo 添加那些方法
 4. 添加权限
```
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
  
    <uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```
##### plugin 项目 
 1. 在plugin_demo\build.gradle 添加依赖 classpath "com.qihoo360.replugin:replugin-host-gradle:2.3.3"
   
 2. app\build.gradle  
	1. 添加依赖  implementation "com.qihoo360.replugin:replugin-host-lib:2.3.3"
	1. 底部添加插件信息
		```
		apply plugin: 'replugin-plugin-gradle'
		repluginPluginConfig {
			pluginName = "dgplugin"   /*  插件别名  自己定义喽*/
			hostApplicationId = "com.replugin.demo.host"  /*  宿主host  应用id*/
			hostAppLauncherActivity = "com.replugin.demo.host.MainActivity"
		}
		```
 3. AndroidManifest.xml中定义别名  
	```
				<meta-data
						android:name="com.qihoo360.plugin.name"
						android:value="dgplugin" />
	```		
 4. 添加权限  
	```
					<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
		<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
		<uses-permission android:name="android.permission.INTERNET"/>
		<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
		<uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
		<uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
		<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
		<uses-permission android:name="android.permission.VIBRATE"/>
		<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
	```
	
 5. 对这个plugin 项目进行打包，打包时记得勾选V1 V2。
 
##### 正式
1. 将打包好的插件apk进行加密(必须、HostUtil)，,将获取插件apk的方式改为下载,不能放置在 main\assets\external\ 下。比如初次获得版本号为1的插件 配置信息，见setPLugin1，
然后在 MainActivity 中进行解密 安装， 在  StartGameActivity 中调用成功。
	```
        Intent intent = new Intent();
        // 在插件的 AndroidManifest.xml 里定义了别名， 这里必须以别名的方式启动
        intent.setComponent(new ComponentName(pluginName, "**.**.BridgeActivity"));// 请修改为对应插件项目BridgeActivity包路径
         
        intent.putExtra("minBet", 10);// 余额最小值，当小于此值时不能下注，0 不限制
        intent.putExtra("token", LoginUtil.token);
        intent.putExtra("gameId", gameId);
        intent.putExtra("domains", LoginUtil.getDomains());
        intent.putExtra("hideBarrage", false);// 是否隐藏弹幕
        boolean startFlg = RePlugin.startActivityForResult(StartGameActivity.this, intent, REQUEST_CODE_DEMO5);
	```  

    
2. 关于卸载插件:只要是进入了游戏 或者 刚刚 安装了插件， 然后进行卸载都是不会立即生效的，它会在你再次进入的时候 自行卸载。如果是之前安装了插件， 再次重新进入app,没有运行插件， 那么卸载是可以立即生效的。  

3. 关于插件别名，在这三个地方 plugin项目的AndroidManifest.xml和build.gradle 还有 host 项目的 PluginConstant.pluginAlias，请保持一致。

##### 提示
1. targetSdkVersion 不支持29 及以上版本。
2.  关于gradle 的配置的小技巧

android 版本 | gradle | gradle 插件
----|------|----
25 | gradle-4.1-all   | 3.0.1 不能是更高的版本
26+ | gradle-4.4-all.zip   | 3.1.0
 +| gradle-4.6-all.zip  | 3.2.0
 +| gradle-4.10.1-all.zip   | 3.3.0
 +| gradle-5.1.1-all.zip   | 3.4.0
 +| gradle-5.4.1-all.zip   | 3.5.0 